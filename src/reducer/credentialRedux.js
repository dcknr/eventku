import { createStore } from 'redux'
export const types = {
    userName: 'userName',
    dataWorkshop: 'dataWorkshop',
    dataKursus: 'dataKursus',
    dataSeminar: 'dataSeminar',
    dataEvent:'dataEvent',
    dataKategori: 'dataKategori',
    isLoading: 'isLoading',
    isLoggedIn: 'isLoggedIn',
    isError: 'isError',
  }

export const actionCreators = {
    userName: (item) => {
      return { type: types.userName, payload: item }
    }, 
    dataWorkshop: (item) => {
      return { type: types.dataWorkshop, payload: item }
    },
    dataKursus: (item) => {
        return { type: types.dataKursus, payload: item }
    },
    dataSeminar: (item) => {
        return { type: types.dataSeminar, payload: item }
    },
    dataEvent: (item) => {
      return { type: types.dataEvent, payload: item }
    },
    dataKategori: (item) => {
      return { type: types.dataKategori, payload: item }
    },
    isLoading: (item) => {
        return { type: types.isLoading, payload: item }
    },
    isLoggedIn: (item) => {
        return { type: types.isLoggedIn, payload: item }
    },
    isError: (item) => {
        return { type: types.isError, payload: item }
    }
  }

  const initialState = {
    userName: '',
    dataWorkshop: {},
    dataKursus: {},
    dataSeminar: {},
    dataEvent: {},
    dataKategori: {},
    isLoading: true,
    isLoggedIn: false,
    isError: false,
  };
  
export const reducerCredential = (state = initialState, action) => {
    const { userName,dataWorkshop,dataKursus,dataSeminar,dataEvent,dataKategori,isLoading,isLoggedIn,isError } = state
    const { type, payload } = action
  
    switch (type) {
      case types.userName: 
      return {
        ...state,
        userName: payload
      }
      case types.dataWorkshop: 
      return {
        ...state,
        dataWorkshop: payload
      }
      case types.dataKursus: 
      return {
        ...state,
        dataKursus: payload
      }
      case types.dataSeminar: 
      return {
        ...state,
        dataSeminar: payload
      }
      case types.dataEvent: 
      return {
        ...state,
        dataEvent: payload
      }
      case types.dataKategori: 
      return {
        ...state,
        dataKategori: payload
      }
      case types.isLoading: 
      return {
        ...state,
        isLoading: payload
      }
      case types.isLoggedIn: 
      return {
        ...state,
        isLoggedIn: payload
      }
      case types.isError: 
      return {
        ...state,
        isError: payload
      }
    }
    return state
  }
const storeCredential = createStore(reducerCredential)
export default storeCredential