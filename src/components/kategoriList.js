import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    TouchableOpacity
} from 'react-native';

export default class eventList extends Component {
    render() {
        let item = this.props.data;
        return (
            <TouchableOpacity>
                <Image
                    style={styles.iconCategory}
                    source = {{ uri: `${item.icon_kategori}` }}
                />
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    iconCategory: {
        width: 150,
        height: 150,
        borderRadius: 10,
        margin: 10,
    },
});
