import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    View,
    Dimensions
} from 'react-native';



export default class footer extends Component {
    render() {
        return (
            <View style={styles.footerContainer}>
                <Image
                    style={styles.logoFooter}
                    source = {require("../../images/logo_biru.png")}
                />
            </View>
        )
    }
}
const {width : WIDTH} = Dimensions.get('window')
const styles = StyleSheet.create({
    
    footerContainer: {
        alignItems: 'center',
        backgroundColor: '#D0D2FF',
        justifyContent: 'center',
        padding: 10,
        width: WIDTH,
    },
    logoFooter: {
        width: 50,
        height: 50,
        borderRadius: 10,
        marginTop: 10
    },
});
