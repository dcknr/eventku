import React, { Component } from 'react';
import {
    Dimensions,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';

export default class eventList extends Component {
    render() {
        let item = this.props.data;
        let title = this.props.title;
        return (
            <TouchableOpacity style={styles.content} onPress={this.props.openDetail}>
                <Image style={styles.headerImageContent} source = {{ uri: `${item.foto_cover}` }} />
                    <View style={styles.rowMarginHeader}>
                        <Text style={styles.textDescriptionEvent}>{title}</Text>
                            <View style={styles.bgFree}>
                                <Text style={styles.textFree}>{item.berbayar === "free" ?"GRATIS":"BERBAYAR"}</Text>
                            </View>
                            </View>
                                <Text style={styles.textJudulEvent}>{item.nama_event}</Text>
                                 <Text style={styles.textDescriptionEvent}>{item.tanggal_mulai_2}</Text>                                 
            </TouchableOpacity>
        )
    }
}

const WIDTH = Dimensions.get('window').width
const styles = StyleSheet.create({
    headerImageContent: {
        width: '100%',
        height: 186,
        borderTopLeftRadius:20,
        borderTopRightRadius:20
    },
    content:{
        elevation: 6,
        backgroundColor: '#D8E7FF',
        width: WIDTH - 50,
        margin: 15,
        borderRadius: 20,
    },
    textDescriptionEvent:{
        fontSize: 18,
        margin: 10, 
    },
    textJudulEvent:{
        fontSize: 18,
        textAlign:'left',
        margin: 10,
        color: "#0061FF"
    },
    rowMarginHeader:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    bgFree:{
        backgroundColor: "#0061FF",
        padding: 5,
        margin: 10,
        borderRadius: 20
    },
    textFree:{
        fontSize: 12,
        color: "#FFF"
    },
    textTentangEventku:{
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: 16,
        padding: 10
    }
});
