import React, { Component } from 'react';
import { connect } from 'react-redux'
import { TextInput, StyleSheet, Text, Image, View, Dimensions, TouchableOpacity} from 'react-native';
import storeCredential,{actionCreators}  from '../reducer/credentialRedux'

const mapStateToProps = (state) => ({
    userName: state.userName,
    isLoggedIn: state.isLoggedIn,
    isError: state.isError,
})

class login extends Component  {
    constructor(props) {
        super(props)
        this.state = {
          userName: '',
          password: '',
        }
      }
    loginHandler() {
    if( this.state.password=='12345678'){
          storeCredential.dispatch(actionCreators.isError(false));
          storeCredential.dispatch(actionCreators.userName(this.state.userName));
          storeCredential.dispatch(actionCreators.isLoggedIn(true));
          this.props.navigation.navigate('Drawer')
        }else{
          storeCredential.dispatch(actionCreators.isError(true));
        }
      }
      
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image
                        style={styles.logo}
                        source = {require("../../images/logo_biru.png")}
                    />
                    <Text style={styles.textLogin}>Login</Text>
                </View>         
                <View style={styles.contentContainer}>
                    <View style={styles.eventContainer}>
                        <View style={styles.form}>
                            <Text style={styles.textInput}>Username / Email</Text>
                            <TextInput
                            style={styles.formInput}
                            onChangeText={userName => this.setState({ userName })}
                            />
                            <Text style={styles.textInput}>Password</Text>
                            <TextInput secureTextEntry={true}
                             onChangeText={password => this.setState({ password })}
                            style={styles.formInput}
                            />
                              <Text style={this.props.isError ? styles.errorText : styles.hiddenErrorText}>Password Salah</Text>
                        </View>
                        <View style={styles.formButton}>
                            {/* Button Masuk */}
                            <TouchableOpacity style={styles.buttonMasuk} onPress={() => this.loginHandler()}>
                                <Text style={styles.textButton}>Masuk</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
          );
    }
}

export default connect(mapStateToProps)(login)

const WIDTH =Dimensions.get('window').width
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#E0F9FE',
    },
    contentContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
       
    },
    eventContainer:{
        elevation: 3,
        backgroundColor: '#F0F9F8',
        width: WIDTH - 50,
        borderRadius: 20,
        marginBottom: 10,
        marginTop: 15,
        backgroundColor:'#D0D2FF'
    },
    logoContainer: {
        alignItems: 'center',
    },

    logo: {
        resizeMode: "contain",
    },

    textLogin:{ 
        fontSize: 24, 
        marginTop: 50,
        color: "#0061FF",
        fontWeight: 'bold'

    },

    form: {
        marginTop: 40,
        alignItems: 'center',
        justifyContent: 'center',
    },

    formInput:{ 
        height: 40, 
        borderColor: "#003366",
        borderWidth: 1,
        marginBottom:20,
        width: WIDTH - 75,
        borderRadius: 15,
        backgroundColor:'#F8F2F2',
        padding: 10 
    },

    textInput:{ 
        color: "#003366",
        marginBottom:5,
        alignItems: "flex-start",
        fontSize: 18, 
        color:'#002D76'
    },

    formButton: {
        marginTop: 10,
        marginBottom: 20,
        alignItems: "flex-end",
        margin: 20
    },

    textButton:{ 
        color: "#FFFFFF",
        fontSize: 18, 
    },

    textInputButton:{ 
        fontSize: 20,
        color: "#3EC6FF",
        marginTop:20,
        marginBottom:20,
        alignSelf: "center"
    },

    buttonMasuk: {
        alignItems: "center",
        backgroundColor: "#002D76",
        padding: 10,
        borderRadius:15,
        width: 100,
        justifyContent: 'center',
       
    },
    errorText: {
        color: 'red',
        textAlign: 'center',
        marginBottom: 16,
      },
    hiddenErrorText: {
        color: 'transparent',
        textAlign: 'center',
        marginBottom: 16,
      }
});
