import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, Image, View, TouchableOpacity, ActivityIndicator, Dimensions, Linking} from 'react-native';
import Axios from 'axios';
import HTML from 'react-native-render-html';
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../components/navbar';
import Footer from '../components/footer'; 
import storeCredential,{actionCreators}  from '../reducer/credentialRedux';
import { PacmanIndicator } from 'react-native-indicators';


const mapStateToProps = (state) => ({
    dataEvent: state.dataEvent,
    isLoading: state.isLoading,
    isError: state.isError,
})

class detail extends Component  {

    // Mount Fungsi Get
    componentDidMount() {
        storeCredential.dispatch(actionCreators.isLoading(true));
        storeCredential.dispatch(actionCreators.isError(false));
        this.getEvent()
    }

    currencyFormat(num) {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };
 
    // Get Data Workshop
    getEvent = async () => {
        try {
            let slug        = this.props.route.params.slug
            const response  = await Axios.get(`https://inkubiz.id/on/api/sinau/event/detail/${slug}` )
            storeCredential.dispatch(actionCreators.dataEvent(response.data.data));
            storeCredential.dispatch(actionCreators.isError(false));
            storeCredential.dispatch(actionCreators.isLoading(false));
        } 
        catch (error) {
            storeCredential.dispatch(actionCreators.isError(true));
            storeCredential.dispatch(actionCreators.isLoading(false));
        }
    }

    render(){
        let event = this.props.dataEvent
        //  If load data
        if (this.props.isLoading) {
            return (
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
                <PacmanIndicator size={80} color='#0061FF' />
            </View>
            )
        }
        // If data not fetch
        else if (this.props.isError) {
            return (
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                <Text>Terjadi Error Saat Memuat Data</Text>
            </View>
            )
        }

        // If data finish load
        return (
            <View style={styles.container}>
                <Navbar navAction={()=>this.props.navigation.navigate('Drawer')} isChildPage="true" />
                <ScrollView vertical>
                    <View style={styles.contentContainer}>
                        <Image
                            style={styles.headerImageContent}
                            source = {{ uri: `${event.foto_cover}` }}
                        />
                        <View style={styles.penyelenggaraContainer}>
                            <View style={styles.penyelenggaraTitle}>
                                <Text style={{fontSize: 22}}>Penyelenggara</Text>
                                <Text style={{fontSize: 20, color:"#003366"}}>{event.penyelenggara}</Text>
                            </View>
                            <View style={styles.rowMargin}>
                                <Icon style={{fontSize:90, color:"#003366", borderRadius: 10}} name="account-circle"></Icon>
                                <View style={styles.columnMargin}>
                                    <Text style={{fontSize: 24, fontWeight:'bold'}}>{event.user_fullname}</Text>
                                    <Text style={{fontSize: 18,fontWeight:'bold', marginTop:10}}>{event.user_email}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.eventContainer}>
                            <Text style={[styles.titleContent,{ color:"#0061FF"}]}>{event.nama_event}</Text>
                                <HTML 
                                 html={"<h5>" + event.deskripsi_event + "</h5>" } 
                                style={{fontSize:23}}
                                textSelectable={true}
                                uri=""
                                imagesMaxWidth={Dimensions.get('window').width-80} />
                        </View>
                        <View style={styles.eventContainer}>
                            <Text style={styles.titleContent}>
                                Harga Tiket
                            </Text>
                            {event.berbayar === "free" ?
                                (<Text style={[styles.textContent,styles.textHarga]}>
                                    FREE
                                </Text>
                                ) 
                                :   
                                (<Text style={[styles.textContent,styles.textHarga]}>
                                    {this.currencyFormat(Number(event.harga))}
                                </Text>)
                            }
                            <Text style={styles.titleContent}>
                                Lokasi
                            </Text>
                            <Text style={styles.textContent}>
                                {event.alamat_event}
                            </Text>
                            <Text style={styles.textContent}>
                                {event.kota}, Provinsi {event.provinsi}
                            </Text>

                            <Text style={styles.titleContent}>
                                Waktu
                            </Text>
                            {event.tanggal_mulai_2 === event.tanggal_selesai_2 ?
                                (<Text style={styles.textContent}>
                                    {event.tanggal_mulai_2}
                                </Text>
                                ) 
                                :   
                                (<Text style={styles.textContent}>
                                    {event.tanggal_mulai_2} - {event.tanggal_selesai_2}
                                </Text>)
                            }
                            {event.jam_mulai === event.jam_selesai ?
                                (<Text style={styles.textContent}>
                                    {event.jam_mulai}
                                </Text>
                                ) 
                                :   
                                (<Text style={styles.textContent}>
                                    {event.jam_mulai} - {event.jam_selesai}
                                </Text>)
                            }
                            
                        </View>    
                        <TouchableOpacity style={styles.bgButton} onPress={ ()=>{ Linking.openURL(event.jenis_pendaftaran_value)}}>
                                <Text style={{fontSize: 18, color: "#FFF", textAlign: "center", marginTop:12}}>BOOKING TICKET</Text>
                        </TouchableOpacity>
                    </View>
                    <Footer/>
                </ScrollView>
            </View>
          );
    }
}
export default connect(mapStateToProps)(detail)
const WIDTH = Dimensions.get('window').width
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#E0F9FE',
    },
    contentContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
    },
    penyelenggaraContainer:{
        elevation: 3,
        backgroundColor: '#FFFFFF',
        width: WIDTH - 50,
        height: 175,
        borderRadius: 20,
        marginBottom: 10,
        marginTop: 15,
        justifyContent:"space-around"
    },
    penyelenggaraTitle:{
        alignItems: 'center',
        margin:10
    },
    eventContainer:{
        width: WIDTH - 50, 
        marginBottom: 10,
        marginTop: 15,
    },
    titleContent:{
        marginTop: 20, 
        marginBottom: 5, 
        fontSize: 24, 
    },
    textContent:{
        fontSize: 18, 
        marginLeft: 30
    },
    textHarga:{
        color:"#0061FF", 
        fontWeight:"bold",
        fontSize: 20, 
    },
    rowMargin:{
        flexDirection: 'row',
        margin:5,
        paddingLeft: 10,
       
    },
    columnMargin: {
        flexDirection: 'column',
        alignSelf: "center",
        justifyContent:"space-around"
    },
    headerImageContent: {
        marginTop: 25,
        width: WIDTH - 50,
        height: 400,
        alignSelf: "center",
        borderRadius: 20,
    },
    bgButton:{
        backgroundColor: "#0061FF",
        borderRadius: 20,
        width: WIDTH - 50,
        height: 50,
    },
});
