import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, Image, View, Dimensions, Linking} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// import component
import Navbar from '../components/navbar';
import Footer from '../components/footer'; 

const WIDTH = Dimensions.get('window').width

export default class about extends Component  {

    render(){
        return (
            <View style={styles.container}>
               <Navbar navAction={()=>this.props.navigation.toggleDrawer()} />
                <ScrollView vertical>
                    <View style={styles.contentContainer}>
                        <View style={styles.eventContainer}>
                            <Image
                                style={styles.headerImageContent}
                                source = {require("../../images/logo_biru.png")}
                            />
                            <Text style={styles.textTentangEventku}>Eventku adalah suatu platform teknologi digital yang menampilkan suatu image dalam bentuk gambar, foto ataupun video yang menampilkan usaha UMKM dan UKM yang sedang bertumbuh dan siap untuk berkembang menjadi besar.</Text>
                            <Text style={styles.textTentangEventku}>Platform ini menekankan pada pengembangan usaha para UMKM dan UKM yang keberadaannya sangat banyak di Indonesia.</Text>
                            <Text style={[styles.textTentangEventku,{fontWeight:'bold', paddingBottom:2}]}>Founder</Text>
                            <View style={styles.rowMargin}>
                               <Icon style={{fontSize:90, color:"#003366", borderRadius: 10}} name="account-circle"></Icon>
                                <View style={styles.columnMargin}>
                                    <Text style={styles.textFounderEventku}>Anri Dicky Septiawan</Text>
                                    <Text style={styles.textFounderEventku}>@dcknr</Text>
                                </View>
                            </View>
                            <View style={styles.rowMargin}>
                                <Icon style={{fontSize:90, color:"#003366", borderRadius: 10}} name="account-circle"></Icon>
                                <View style={styles.columnMargin}>
                                    <Text style={styles.textFounderEventku}>Said Achmad</Text>
                                    <Text style={styles.textFounderEventku}>@Said1128</Text>
                                </View>
                            </View>
                            <Text style={{ padding: 20,color: "#003366", fontSize:14}}>Sumber Web/API : <Text style={{padding: 20,color: "#003366", fontSize:14}} onPress={ ()=>{ Linking.openURL('https://sinauinspira.id/')}}>Sinauinspira.id</Text> </Text>
                        </View>
                    </View>
                    <Footer/>
                </ScrollView>
            </View>
          );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#E0F9FE',
    },
    contentContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
    },
    eventContainer:{
        elevation: 3,
        backgroundColor: '#F0F9F8',
        width: WIDTH - 50,
        borderRadius: 20,
        marginBottom: 10,
        marginTop: 15,
    },
    rowMargin:{
        flexDirection: 'row',
        margin:5,
        paddingLeft: 10,
       
    },
    columnMargin: {
        flexDirection: 'column',
        alignSelf: "center",
    },
    textFounderEventku:{
        fontSize: 20,
        textAlign:"left",
        color: "#003366"
    },
    headerImageContent: {
        marginTop: 25,
        width: 200,
        height: 200,
        alignSelf: "center",
        borderRadius: 20,
    },
    textTentangEventku:{
        fontSize: 20,
        padding: 20,
        color: "#003366"
    }

});
